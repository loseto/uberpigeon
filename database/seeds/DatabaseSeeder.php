<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pigeons')->insert([
            'name' => 'Antonio',
            'speed' => 70,
            'range' => 600,
            'cost' => 2,
            'downtime' => 2
        ]);

        DB::table('pigeons')->insert([
            'name' => 'Bonito',
            'speed' => 80,
            'range' => 500,
            'cost' => 2,
            'downtime' => 3
        ]);

        DB::table('pigeons')->insert([
            'name' => 'Carillo',
            'speed' => 65,
            'range' => 1000,
            'cost' => 2,
            'downtime' => 3
        ]);

        DB::table('pigeons')->insert([
            'name' => 'Alejandro',
            'speed' => 70,
            'range' => 800,
            'cost' => 2,
            'downtime' => 2
        ]);
    }
}
