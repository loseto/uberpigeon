<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = ['departure_time', 'distance', 'deadline', 'cost', 'pigeon_id'];

    public function pigeon()
    {
        return $this->belongsTo('App\Pigeon');
    }
}
