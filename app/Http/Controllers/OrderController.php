<?php

namespace App\Http\Controllers;


use App\Http\Requests\OrderRequest;
use App\Order;
use App\Services\CreateOrderService;
use App\Services\SearchPigeonService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Order::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        // Retrieve the validated input data
        $validator = Validator::make($request->all(), [
            'distance' => 'required|numeric|min:1|max:9999999',
            'deadline' => 'required|date_format:H:i'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $deadline = new Carbon($request->input('deadline'));
        if ($deadline->lte(Carbon::now())) {
            return response()->json(['error' => 'The time must be in the future'], 400);
        }

        // Filter the pigeons based on the SearchPigeonService strategies
        $searchPigeonService = new SearchPigeonService($request);
        $filteredPigeon = $searchPigeonService->filter();

        // Create the order
        $createOrderService = new CreateOrderService($filteredPigeon, $request);
        $order = $createOrderService->submitOrder();

        // Return the new order
        if ($order != null && !empty($order)) {
            return response()->json($order, 201);
        } else {
            return response()->json(['error' => 'No pigeons available for this order'], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(Order::find($id));
    }
}