<?php

namespace App\Services;

use App\Order;
use App\Pigeon;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CreateOrderService
{
    protected $pigeons;
    protected $distance;
    protected $deadline;

    /**
     * Strategy constructor.
     * @param $pigeons
     * @param $distance
     * @param $deadline
     */
    public function __construct(Builder $pigeonBuilder, Request $request)
    {
        $this->pigeons = $pigeonBuilder;
        $this->distance = $request->input('distance');
        $this->deadline = Carbon::parse($request->input('deadline'));
    }

    /**
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|null
     */
    function submitOrder()
    {
        // Retrive the pigeons filtered by SearchPigeonService strategies
        $pigeonCollection = $this->pigeons->get();
        if (count($pigeonCollection) <= 1) return null;

        // For each pigeon evaluate the associated orders timing
        foreach ($pigeonCollection as $pigeon) {
            $currentPigeonOrder = Order::where('pigeon_id', $pigeon->id)->orderBy('departure_time', 'desc')->first();
            if (!$currentPigeonOrder) {
                // If this pigeons is free choosing it
                return $this->make(new Carbon(), $pigeon);
            }

            // Evaluate the timing of the current order for this pigeon
            $previousDepartureTime = new Carbon($currentPigeonOrder->departure);
            $previousDistance = $currentPigeonOrder->distance;

            // Evaluate the flying time for the current and requested order
            $previousFlyingTime = $this->toSecond($previousDistance / $pigeon->speed);
            $currentOrderFlyingTime = $this->toSecond($this->distance / $pigeon->speed);

            // Calculate the new order delivery time and departure time
            $deliveryTime = $previousDepartureTime->copy()->addSeconds($previousFlyingTime * 2)->addSeconds($this->toSecond($pigeon->downtime))->addSeconds($currentOrderFlyingTime);
            $departureTime = $deliveryTime->copy()->subSeconds($currentOrderFlyingTime);

            // Evaluate if the order delivery time respects the deadline and the same-day
            if ($deliveryTime->lte($this->deadline) && $deliveryTime->diffInDays(Carbon::now()) == 0) {
                return $this->make($departureTime, $pigeon);
            }
        }
        return null;
    }

    /**
     * @param $hours
     * @return int
     */
    private function toSecond($hours)
    {
        return (int)($hours * 3600);
    }

    /**
     * @param Carbon $departureTime
     * @param Pigeon $pigeon
     * @return mixed
     */
    private function make(Carbon $departureTime, Pigeon $pigeon)
    {

        $cost = $pigeon->cost * $this->distance;

        $order = Order::create([
            'departure_time' => $departureTime->toDateTimeString(),
            'distance' => $this->distance,
            'deadline' => $this->deadline->toDateTimeString(),
            'cost' => $cost,
            'pigeon_id' => $pigeon->id
        ]);

        return $order;
    }
}