<?php
/**
 * Created by PhpStorm.
 * User: claudio
 * Date: 04/08/18
 * Time: 11:32
 */

namespace App\Services;


use App\Pigeon;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SearchPigeonService
{
    protected $deadline;
    protected $distance;

    /**
     * SearchPigeonService constructor.
     * @param $deadline
     * @param $distance
     */
    public function __construct(Request $request)
    {
        $this->deadline = Carbon::parse($request->input('deadline'));
        $this->distance = $request->input('distance');

        /**
         * Future expansions examples
         *
         * Read the weight of the income order request
         * $this->weight = $weight
         */
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function filter()
    {
        $pigeon = new Pigeon();
        $pigeonsQueryBuilder = $pigeon->newQuery();

        // Filter for distance
        $pigeonsQueryBuilder->where('range', '>=', $this->distance);

        /**
         * Future expansions examples
         *
         * Filter the pigeons per maximum weight
         * $pigeonsQueryBuilder->where('maximum_weight', '>=', $giver_weight);
         *
         * Filter the pigeons for sick leave
         * $pigeonsQueryBuilder->where('sick_leave', 0);
         *
         */

        return $pigeonsQueryBuilder;
    }
}