<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pigeon extends Model
{
    protected $table = 'pigeons';
    public $timestamps = false;

    public function orders()
    {
        return $this->hasMany('App\Order');
    }
}
