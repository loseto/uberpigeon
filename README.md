# UberPigeon
Project based on Laravel Lumen 5.6

# Requirements
* MySql database
* PHP >= 7.1.3
* PDO PHP Extension
* Mbstring PHP Extension

# Setup
1. Clone the repository
2. Edit MySql connection (`.env`)
3. Create/Reset database tables: `php artisan migrate:refresh`
4. Seed pigeons table: `php artisan db:seed`
5. Start the http server: `php -S localhost:8000 -t public`

# Available API
* List all the orders 
```
GET /orders
Response Codes: Success (200)
```

* Show one order 
```
GET /orders/{id}
URL Params: id (integer)
Response Codes: Success (200)
```

* Submit a new order
```
POST /orders
Payload: deadline (hh:mm), distance (integer) 
Response Codes: Created (201), Bad Request (400), Forbidden (403)
```

# Possible improvements
* The pigeon choosing strategy **doesn’t care about the best delivery time** but is focused only on respecting the deadline.
* Improve timezone management.
* Versioning the api (ex. `/api/v1/orders`, `/api/v2/orders`)

# Future expansions
The pigeons filter strategy is defined into the `filter()` method of  `app/Services/SearchPigeonService` class.
All the expansions on the choosing strategy can be done in this class, in most cases without modifying other project parts.